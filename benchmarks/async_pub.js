const path = require('path')
const IPFS = require('ipfs')
const IpfsApi = require('ipfs-http-client');
const OrbitDB = require('orbit-db')
const {docopt}  = require('docopt');

doc =`
Publish on a topic via simple nodejs broker on top of IPFS

Usage:
    async_pub.js topic -n <name> -m <msg> -i <iter>
`;

var arguments = docopt(doc);

const ipfs = new IpfsApi('localhost', 5001)

// handler

console.log(arguments)
var topic = arguments['<name>']
var msg = arguments['<msg>']
var iter = arguments['<iter>']
var i = 0

async function main () {
  try {
    while (i < iter) {
      ipfs.pubsub.publish(topic, Buffer.from(msg+"_"+`${i}`), (err) => {
        if (err !== undefined && err !== null)
          console.log (`Error: ${err}`)
      })
      console.log(`Sucessfully send messsage '${i}' to topic '${topic}'`)
      i++
    }
  }
  catch (e) {
    console.error(`Error on ${topic}`, e)
  }
}

main()
