/* Sometimes this implementation hangs arround 200 iterations if this happens use the ipfs client
 * the only difference is that the reception timesatamps are not shown
*/

const IPFS = require('ipfs')
const IpfsApi = require('ipfs-http-client');
const OrbitDB = require('orbit-db')
const {docopt}  = require('docopt');

doc =`
Test subscriptions for pubsub

Usage:
    test_sub.js topic <name>
`;

var arguments = docopt(doc);
var i = 0;
// Create IPFS instance

const ipfs = new IpfsApi('localhost', 5001)
// handler
const receiveMsg = (msg) => console.log(`${i++} At ${Date.now()} - Received: ${msg.data.toString()}`)

console.log(arguments)
var topic = arguments['<name>']

async function main () {
  try {
    ipfs.pubsub.subscribe(topic, receiveMsg, (err) => {
      if (err !== undefined && err !== null)
        console.log (`Error: ${err}`)
    })
    console.log(`Subscribed to ${topic}`)
  }
  catch (e) {
    console.error(`Error on ${topic}`, e)
  }
}

main()
