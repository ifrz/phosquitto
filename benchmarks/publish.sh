#!/bin/sh

for i in $(seq $1)
do
  ipfs pubsub pub $3 hello_from_$2_packet${i}_$(($(date +%s%N)/1000000));
done
