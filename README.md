This project aiming to implement a fully integrated solution for IoT communication over ipfs, attempting to integrate the following stack.

PubSub on ipfs. https://blog.ipfs.io/25-pubsub/

In order to communicate peers (nodes) among each others

OrbitDB. https://github.com/orbitdb/orbit-db

To get persistence of the messages

Implement the 3 levels of QoS defined on MQTT via pubsub and provided a http API rest allowing working outside ipfs

Example of the cli

nodeA

`node phosquitto_sub.js topic test_foo`

return an address and wait for messages

the address is the orbitdb associate topic db

nodeB

`phosquitto_pub.js topic address -n test_foo -m Hello`

send the message via pubsub and store the message in the topic db

The web-cli allows to connect an external ipfs instance (peer) and works outside of the ipfs network




This project aims to integrate several tools/technologies in order to test the performance and viability of an IPFS based decentralized architecture for IoT, not a fully functional solution for an IoT enviroment