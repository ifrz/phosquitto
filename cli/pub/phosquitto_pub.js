/*
    * ToDo implement QoS 0,1,2 (oficial mqtt)
    * Basic communication between 2 peers
    * Create and subscribe topic and simple messaging
    * Try to use jsmqtt on top of this
    *
*/

const path = require('path')
const IPFS = require('ipfs')
const IpfsApi = require('ipfs-http-client');
const OrbitDB = require('orbit-db')
const {docopt}  = require('docopt');

// API rest

doc =`
Publish on a topic via simple nodejs broker on top of IPFS and MQTT.

Usage:
    phosquitto_pub.js topic <address> -n <name> -m <msg> [--QoS=<0|1|2>]
    phosquitto_pub.js -h | --help

`;

var arguments = docopt(doc);
/*
const ipfsOptions = {
    repo: './phosquitto/topic/ipfs',
    EXPERIMENTAL: {
            pubsub: true
    },
    config: {
      API: {
        HTTPHeaders: {
          Access-Control-Allow-Origin: [
            "http://127.0.0.1:8888"
          ]
        }
      },
        Addresses: {
            API: '/ip4/127.0.0.1/tcp/5002',
            Swarm: ['/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'],
            Gateway: '/ip4/0.0.0.0/tcp/4003'
        }
    }
}*/

// Create IPFS instance
//const ipfs = new IPFS(ipfsOptions)

const ipfs = new IpfsApi('localhost', 5001)

// handler
//const receiveMsg = (msg) => console.log(msg.data.toString())
// topic specified by the client
// [{ host: 'localhost', port: 4001 }, ... ] then use the mqttjs library
console.log(arguments)
var address = arguments['<address>']
var topic = arguments['<name>']
var msg = arguments['<msg>']

//ipfs.on('ready', async () => {
async function main () {
    try {
      //let subs = await ipfs.pubsub.subscribe(topic, receiveMsg);
      let broker
      const { id } = await ipfs.id()
      console.log(`Peer ID: ${id}`)
      const phosquitto = await OrbitDB.createInstance(ipfs, {
        directory: './phosquitto/topic'
      })
      console.log(path.join(address, '/', topic))
      broker = await phosquitto.open(path.join(address, '/', topic))//eventlog(topic, access)
      //console.log(`created topic ${topic}`)
      //await broker.load()
      //console.log(`Subscribe to '${topic}`)
      const hash = await broker.add(msg)
      console.log("buffer.")
      console.log(Buffer.from(msg))
      console.log(`Topic Msg. ${msg}`)
      //let publication = await ipfs.pubsub.publish(topic, receiveMsg);
      ipfs.pubsub.publish(topic, Buffer.from(msg), (err) => {
        if (err !== undefined && err !== null)
          console.log (`Error: ${err}`)
      })
      console.log(`Sucessfully send '${msg}' to topic '${topic}' - ${hash}`)

    }
    catch (e) {
      console.error(`Error on ${topic}`, e)
    }
//});
}

main()
