/*
    * ToDo implement QoS 0,1,2 (oficial mqtt)
    * Basic communication between 2 peers
    * Create and subscribe topic and simple messaging
    * Try to use jsmqtt on top of this
    *
*/

const IPFS = require('ipfs')
const IpfsApi = require('ipfs-http-client');
const OrbitDB = require('orbit-db')
const {docopt}  = require('docopt');

doc =`
Subscribe to a topic via simple nodejs broker on top of IPFS and MQTT.

Usage:
    phosquitto_sub.js topic <name> [--QoS=<0|1|2>]
    phosquitto_sub.js -h | --help

`;

var arguments = docopt(doc);
/*
const ipfsOptions = {
    repo: './phosquitto/topic/ipfs',
    EXPERIMENTAL: {
            pubsub: true
    },
    config: {
        API: {
          HTTPHeaders: {
            'Access-Control-Allow-Origin': [
              "http://127.0.0.1:8888"
            ]
          }
        },
        Addresses: {
            API: '/ip4/127.0.0.1/tcp/5001',
            Swarm: ['/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'],
            Gateway: '/ip4/0.0.0.0/tcp/4003'
        }
    }
}*/

// Create IPFS instance
//const ipfs = new IPFS(ipfsOptions)
const ipfs = new IpfsApi('localhost', 5001)
// handler
const receiveMsg = (msg) => console.log(`Received: ${msg.data.toString()}`)

console.log(arguments)
var topic = arguments['<name>']

async function main () {
//ipfs.on('ready', async () => {
    try {
      ipfs.pubsub.subscribe(topic, receiveMsg, (err) => {
        if (err !== undefined)
          console.log (`Error: ${err}`)
      })
      console.log(`Subscribed to ${topic}`)
      let broker
      const { id } = await ipfs.id()
      console.log(`Peer ID: ${id}`)
      const phosquitto = await OrbitDB.createInstance(ipfs, {
        directory: './phosquitto/topic'
      })
      const options = {
      //  overwrite: false,
        accessController: {
          write: [
            '*' // Put the Peer id public key
          ],
        }
      }
      broker = await phosquitto.eventlog(topic, options)
      //console.log(`created topic ${topic}`)
      await broker.load()
      console.log()
      console.log(`Sucessfully created to topic ${topic}`)
      console.log(`Address of the topic: ${broker.address.toString().split('/')[2]}`)
      // Handlers
      broker.events.on('replicated', () => {
      //Emitted when the database has synced with another peer. This is usually a good place to re-query
      //the database for updated results, eg. if a value of a key was changed or if there are new events
      // in an event log.
        console.log(Date.now())
        console.log('Synced with new peer...')
        console.log('Now can close this peer topic db...')
        broker.close()
      })
      broker.events.on('replicate', () => {
      //Emitted before replicating a part of the database with a peer.
        console.log(Date.now())
        console.log('Replicate part of Broker DB with new peer...')
      })
      broker.events.on('write', () => {
      //Emitted after an entry was added locally to the database. hash is the IPFS hash
      //of the latest state of the database
        console.log('New data write in Broker DB...')
      })
      broker.events.on('replicate.progress', (address, hash, entry, progress, have) => {
        console.log('replicating... ')
      })
    }
    catch (e) {
      console.error(`Error on ${topic}`, e)
    }
//});
}

main()
